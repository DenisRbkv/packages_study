import 'package:flutter/material.dart';

class UserModel {
  final String imageUrl;
  final String name;
  final String phone;
  final String email;

  UserModel({
    @required this.imageUrl,
    @required this.name,
    @required this.phone,
    @required this.email,
  });
}
