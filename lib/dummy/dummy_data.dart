import 'package:flutter/foundation.dart';

import 'package:packages_study/models/model.dart';

class DummyData {
  static final List<UserModel> dataList = [
    UserModel(
      imageUrl: 'https://strattonapps.com/wp-content/uploads/2020/02/flutter-logo-5086DD11C5-seeklogo.com_.png',
      name: 'John Test',
      phone: '88005553535',
      email: 'testmail@gmail.com'
    ),
  ];
}