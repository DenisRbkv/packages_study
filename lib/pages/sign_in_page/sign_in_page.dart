import 'package:flutter/material.dart';

import 'package:packages_study/pages/info_page/info_page.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController loginController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void dispose() {
    loginController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: _unfocusAll,
        child: Container(
          color: Colors.grey.withOpacity(0.8),
          child: Column(
            children: <Widget>[
              Spacer(),
              Container(
                height: 280.0,
                margin: const EdgeInsets.symmetric(horizontal: 70.0),
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(
                    width: 2.0,
                    color: Colors.blueGrey,
                  ),
                  color: Colors.white.withOpacity(0.9),
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 15.0),
                    Text(
                      'Sign In',
                      style: TextStyle(fontSize: 24.0),
                    ),
                    SizedBox(height: 15.0),
                    TextField(
                      //decoration: InputDecoration(),
                      autofocus: true,
                      controller: loginController,
                    ),
                    SizedBox(height: 15.0),
                    TextField(
                      //decoration: InputDecoration(),
                      autofocus: true,
                      controller: passwordController,
                    ),
                    SizedBox(height: 45.0),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 25.0),
                      width: double.infinity,
                      height: 35.0,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        color: Colors.blueGrey,
                      ),
                      child: InkWell(
                        onTap: () => Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                              builder: (BuildContext context) => InfoPage(),
                          ),
                        ),
                        child: Text(
                          'OK',
                          style: TextStyle(color: Colors.white, fontSize: 15.0),
                        ),
                      ),
                    ),
                    Spacer(),
                  ],
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }

  void _unfocusAll() {
    FocusScope.of(context).unfocus();
  }
}