import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:packages_study/models/model.dart';

class UserCard extends StatelessWidget {
  final UserModel userModel;

  UserCard(this.userModel);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200.0,
      margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.redAccent.withOpacity(0.7),
      ),
      child: Container(
        margin: const EdgeInsets.all(5.0),
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Row(
            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                height: 120.0,
                width: 120.0,
                child: ClipOval(
                  child: Image.network(userModel.imageUrl),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    userModel.name,
                    style: TextStyle(fontSize: 23.0, fontWeight: FontWeight.bold, color: Colors.blueGrey),
                    textAlign: TextAlign.left,
                  ),
                  Text(
                    'phone: \n${userModel.phone}',
                    style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.grey),
                  ),
                  Text(
                    'email: \n${userModel.email}',
                    style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.grey),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
