import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:packages_study/pages/info_page/widgets/user_card.dart';
import 'package:packages_study/dummy/dummy_data.dart';

class InfoPage extends StatefulWidget {
  @override
  _InfoPageState createState() => _InfoPageState();
}

class _InfoPageState extends State<InfoPage> {
  SharedPreferences _prefs;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Info Page'),
        backgroundColor: Colors.blueGrey,
      ),
      body: Column(
        children: <Widget>[
          UserCard(DummyData.dataList[0]),
        ],
      ),
    );
  }

  void get _getList {
   for (int i = 0; i < DummyData.dataList.length; i++)
     _saveModel(i);
  }

  Future<void> _saveModel(int i) async {
    await _prefs.setString('user+$i+image', DummyData.dataList[i].imageUrl);
  }

  Future<String> _getModel(int i) async {
    return _prefs.getString('user+$i');
  }
}
